@extends('layouts.main')

@section('content')
    <div class="container">
        {{--create grane form garne--}}

        @if($hospitals)

        <form action="{{route('updatehospital')}}" method="POST">

            <div class="form-group">
                <label for="Hospital Name">Hospital Name</label>
                <input type="text" id="name"  value="{{$hospitals['hospital_name']}}" name="name" class="form-group">

            </div>

            <div class="form-group">
                <label for="Hospital image ">Hospital Image</label>
                <input type="text" id="image"value="{{$hospitals['image']}}"  name="image" class="form-group">

            </div>

            <div class="form-group">
                <label for="Hospital location">Hospital Location</label>
                <input type="text" id="location" value="{{$hospitals['location']}}" name="location" class="form-group">

            </div>

            <div class="form-group">
                <label for="Hospital Specialization">Hospital Specialization</label>
                <input type="text" id="specialization" value="{{$hospitals['specialization']}}" name="specialization" class="form-group">

            </div>
            <input type="submit" value="update" name="update" class="btn btn-primary">
            <input type="hidden" name="_token" value="  {{csrf_token()}}">
            <input type="hidden" name="id" value="{{ $hospitals['id']}}">
        </form>
            @endif

    </div>

@endsection
