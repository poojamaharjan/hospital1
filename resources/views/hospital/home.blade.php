@extends('layouts.main')

@section('content')
    <div class="container">

        @if(isset($message))
            <li>{{$message}}</li>

            @endif
        <a href="{{route('addhospital') }}" class="btn btn-primary"> Add new hospital</a>
        <hr>
        <table class="table table-responsive table-bordered">
            <thead>
            <tr>
                <th>S.no</th>
                <th>Hospital_name</th>
                <th>Image</th>
                <th>Location</th>
                <th>Specialization</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($hospitals))
                @foreach($hospitals as $clinic)
            <tr>
                <td>{{ $clinic->id }}</td>
                <td>{{ $clinic->hospital_name }}</td>
                <td>{{ $clinic->image }}</td>
                <td>{{ $clinic->location }}</td>
                <td>{{ $clinic->specialization }}</td>
                <td>
                    <a href="{{ route('edithospital', ['id' => $clinic->id]) }}" class="btn btn-primary">Edit</a>

                    <a href="{{route('deletehospital',['id'=>$clinic->id]) }}" class="btn btn-danger">Delete</a>
                </td>

            </tr>
                @endforeach
                @endif
            </tbody>

        </table>
    </div>
   {{-- define table--}}


    @endsection







