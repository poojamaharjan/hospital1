@extends('layouts.main')

@section('content')
    <div class="container">
        {{--create grane form garne--}}
        <form action="{{route('addhospital')}}" method="POST">

                  <div class="form-group">
                      <label for="Hospital Name">Hospital Name</label>
                      <input type="text" id="name" name="name" class="form-group">

                  </div>

            <div class="form-group">
                <label for="Hospital image ">Hospital Image</label>
                <input type="text" id="image" name="image" class="form-group">

            </div>

            <div class="form-group">
                <label for="Hospital location">Hospital Location</label>
                <input type="text" id="location" name="location" class="form-group">

            </div>

            <div class="form-group">
                <label for="Hospital Specialization">Hospital Specialization</label>
                <input type="text" id="specialization" name="specialization" class="form-group">

            </div>
            <input type="submit" value="create" name="submit" class="btn btn-primary">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>

    </div>

@endsection
