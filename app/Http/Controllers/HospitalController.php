<?php

namespace App\Http\Controllers;


use App\clinic;
use Illuminate\Http\Request;
use Mockery\Exception;
use DB;

class HospitalController extends Controller
{
    //
    public function showHome(){
        try{
            $result=clinic::all();
           // print_r($result);die();
            if(count($result)==0){
                return view('hospital.home')->with(['message' => 'database is empty']);
            }
            else{
                return view('hospital.home')->with(['hospitals'=>$result]);
            }


        }catch(\Exception $e)
        {
            return $e;
        }
    }
    public function showHospitalForm(){
        return view('hospital.create');
    }


        public function insertHospital(Request $request){
           try {
               /*object banaune lagyeko model ko*/

               $hospitalObj=new  clinic();

          /*     first ko name chai database ma j cha tehi halne last ko chai form sanga match hunu parcha*/



               $hospitalObj->hospital_name= $request->input('name');
               $hospitalObj->image= $request->input('image');
               $hospitalObj->location= $request->input('location');
               $hospitalObj->specialization= $request->input('specialization');

               $hospitalObj->save();
               if($hospitalObj->save()){
                   //return view('clinic.home')->with(['message'=>'your data has been store successfully']);
                   return redirect()->route('listhospital')->with(['message'=>'your data has been added sucessfully']);
               }
           }catch(\Exception $e){
               return $e;
           }
        }

        public function getSingleHospital($id){
            try{
                $result=clinic::find($id)->first()->toArray();
                if(count($result)==0){
                    return view('hospital.edit')->with(['message' => 'database is empty']);
                }
                else{
                    return view('hospital.edit')->with(['hospitals'=>$result]);
                }

            }catch(\Exeception $e){
                return($e);
            }

        }



        public function updateHospital(Request $request ){
            try{
                $id=$request->get('id');
                $hospitalObj=clinic::find($id);


                $hospitalObj->hospital_name=$request->input('name');
                $hospitalObj->image=$request->input('image');
                $hospitalObj->location=$request->input('location');
                $hospitalObj->specialization=$request->input('specialization');

                $hospitalObj->save();
                if($hospitalObj->save()){
                    return redirect()->route('listhospital')->with(['message'=>'your data has been update sucessfully']);
                }



            }catch(\Exception $e){
                return $e;
            }
        }


        public function deleteHospital($id){
            try{
                DB::table('clinic')->where('id', '=' , $id)->delete();
                return redirect()->route('listhospital')->with(['message'=>'your data has been delete sucessfully']);

            }catch(Exception $e){
                print_r($e);
            }
        }
}
