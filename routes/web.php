<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/clinic',['uses'=>'HospitalController@showHome','as'=>'listhospital']);
Route::get('/clinic/create',['uses'=>'HospitalController@showHospitalForm','as'=>'addhospital']);
Route::get('/clinic/edit/{id}',['uses'=>'HospitalController@getSingleHospital','as'=>'edithospital']);
Route::get('/clinic/delete/{id}',['uses'=>'HospitalController@deleteHospital','as'=>'deletehospital']);

Route::post('/clinic/create',['uses'=>'HospitalController@insertHospital','as'=>'inserthospital']);
Route::post('/clinic/update',['uses'=>'HospitalController@updateHospital','as'=>'updatehospital']);
